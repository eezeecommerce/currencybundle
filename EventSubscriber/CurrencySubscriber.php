<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26/05/16
 * Time: 14:04
 */

namespace eezeecommerce\CurrencyBundle\EventSubscriber;


use eezeecommerce\CurrencyBundle\Provider\CurrencyProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CurrencySubscriber implements EventSubscriberInterface
{
    protected $currency;

    public function __construct(CurrencyProvider $currency)
    {
        $this->currency = $currency;
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::FINISH_REQUEST => array(
                array("onKernelResponse", 150)
            )
        );
    }

    public function onKernelResponse(FinishRequestEvent $event)
    {
        if (!$event->getRequest()) {
            return;
        }
        $this->currency->loadCurrency();
    }
}