<?php

namespace eezeecommerce\CurrencyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="eezeecommerce\ShippingBundle\Entity\CountryRepository")
 */
class Currency
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;
            
    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\SettingsBundle\Entity\Settings", inversedBy="currencies", fetch="LAZY")
     * @ORM\JoinColumn(name="settings_id", referencedColumnName="id")
     */
    protected $settings;

    /**
     * @ORM\OneToMany(targetEntity="eezeecommerce\SettingsBundle\Entity\Settings", mappedBy="default_currency", fetch="LAZY")
     */
    protected $default_currencies;
    
    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $currency_code;
    
    /**
     * @ORM\Column(type="decimal", precision=19, scale=6)
     */
    protected $exchange_rate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return Currency
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currency_code = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currency_code;
    }

    /**
     * Set exchangeRate
     *
     * @param string $exchangeRate
     *
     * @return Currency
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchange_rate = $exchangeRate;

        return $this;
    }

    /**
     * Get exchangeRate
     *
     * @return string
     */
    public function getExchangeRate()
    {
        return $this->exchange_rate;
    }

    /**
     * Set settings
     *
     * @param \eezeecommerce\SettingsBundle\Entity\Settings $settings
     *
     * @return Currency
     */
    public function setSettings(\eezeecommerce\SettingsBundle\Entity\Settings $settings = null)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Get settings
     *
     * @return \eezeecommerce\SettingsBundle\Entity\Settings
     */
    public function getSettings()
    {
        return $this->settings;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->default_currencies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add defaultCurrency
     *
     * @param \eezeecommerce\SettingsBundle\Entity\Settings $defaultCurrency
     *
     * @return Currency
     */
    public function addDefaultCurrency(\eezeecommerce\SettingsBundle\Entity\Settings $defaultCurrency)
    {
        $this->default_currencies[] = $defaultCurrency;

        return $this;
    }

    /**
     * Remove defaultCurrency
     *
     * @param \eezeecommerce\SettingsBundle\Entity\Settings $defaultCurrency
     */
    public function removeDefaultCurrency(\eezeecommerce\SettingsBundle\Entity\Settings $defaultCurrency)
    {
        $this->default_currencies->removeElement($defaultCurrency);
    }

    /**
     * Get defaultCurrencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDefaultCurrencies()
    {
        return $this->default_currencies;
    }
}
