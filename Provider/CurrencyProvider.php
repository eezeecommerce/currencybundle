<?php

namespace eezeecommerce\CurrencyBundle\Provider;

use Doctrine\ORM\PersistentCollection;
use eezeecommerce\CurrencyBundle\Core\CurrencyManager;
use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use eezeecommerce\CurrencyBundle\CurrencyEvents;
use eezeecommerce\CurrencyBundle\Entity\Currency;
use eezeecommerce\CurrencyBundle\Event\CurrencyEvent;
use eezeecommerce\SettingsBundle\Entity\Settings;
use Doctrine\Common\Collections\Collection;
use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CurrencyProvider
{
    /**
     * @var Settings
     */
    protected $settings;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var CurrencyManager
     */
    private $manager;

    /**
     * CurrencyProvider constructor.
     * @param SettingsProvider $settings loadSettingsBySite
     */
    public function __construct(EventDispatcherInterface $dispatcher,SettingsProvider $settings, CurrencyManager $manager)
    {
        $this->settings = $settings;
        $this->dispatcher = $dispatcher;
        $this->manager = $manager;
    }

    public function loadCurrency()
    {
        if (null !== $this->manager->get()) {
            if (null !== $this->manager->get()->getEntity()->getSettings()) {
                return $this->manager->get();
            }
        }

        $settings = $this->settings->loadSettingsBySite();
        if (null === $settings) {
            return;
        }
        $default = $settings->getDefaultCurrency();

        if ($default instanceof PersistentCollection) {
            $item = new CurrencyItem($default->first());
        }else {
            $item = new CurrencyItem($default);
        }

        $item->getEntity()->setSettings($settings);

        $event = new CurrencyEvent($item);

        $this->dispatcher->dispatch(CurrencyEvents::CURRENCY_SAVE_INITIALISE, $event);

        if ($event->isPropagationStopped()) {
            return;
        }

        if (!$this->manager->check()) {
            $this->manager->set($item);
        } else {
            if ($this->manager->get() != $item) {
                $this->manager->set($item);
            }
        }

        $this->dispatcher->dispatch(CurrencyEvents::CURRENCY_SAVE_COMPLETED, $event);

        return $this->manager->get();
    }
}