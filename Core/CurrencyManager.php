<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/05/16
 * Time: 14:04
 */

namespace eezeecommerce\CurrencyBundle\Core;


use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CurrencyManager
{
    private $session;

    public static $key = "_currency_code";

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function check()
    {
        return $this->session->has(self::$key);
    }

    public function set(CurrencyItem $currency)
    {
        $this->session->set(self::$key, $currency);
    }

    /**
     * @return CurrencyItem
     */
    public function get()
    {
        return $this->session->get(self::$key);
    }
}