<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/05/16
 * Time: 14:13
 */

namespace eezeecommerce\CurrencyBundle\Tests\Event;

use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use eezeecommerce\CurrencyBundle\Event\CurrencyEvent;
use Symfony\Component\EventDispatcher\Event;

class CurrencyEventTest extends \PHPUnit_Framework_TestCase
{
    public function testCurrencyEventInstanceOfEvent()
    {
        $currency = new \ReflectionClass(CurrencyEvent::class);

        $this->assertTrue($currency->isSubclassOf(Event::class));
    }

    public function testEventReturnsInstanceOfCurrency()
    {
        $currency = $this->getMockBuilder(CurrencyItem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event = new CurrencyEvent($currency);

        $this->assertEquals($currency, $event->getCurrency());
    }
}