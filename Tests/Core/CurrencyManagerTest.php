<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/05/16
 * Time: 14:31
 */

namespace eezeecommerce\CurrencyBundle\Tests\Core;

use eezeecommerce\CurrencyBundle\Core\CurrencyManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use eezeecommerce\CurrencyBundle\Entity\Currency;
use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;

class CurrencyManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testCurrencyManagerAcceptsSessionInterface()
    {
        $session = $this->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $manager = new CurrencyManager($session);

        $this->assertNotFalse($manager);
    }

    public function testCurrencyManagerReturnsFalseIfCurrencyDoesNotExist()
    {
        $session = $this->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->once())
            ->method("has")
            ->will($this->returnValue(false));

        $manager = new CurrencyManager($session);

        $this->assertFalse($manager->check());
    }

    public function testCurrencyManagerReturnsTrueIfExists()
    {
        $session = $this->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->once())
            ->method("has")
            ->will($this->returnValue(true));

        $manager = new CurrencyManager($session);

        $this->assertTrue($manager->check());
    }

    public function testCurrencyTestSettingCurrencyEntity()
    {
        $currency = $this->getMockBuilder(Currency::class)
            ->disableOriginalConstructor()
            ->getMock();

        $currencyItem = $this->getMockBuilder(CurrencyItem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $currencyItem->expects($this->any())
            ->method("getEntity")
            ->will($this->returnValue($currency));

        $session = $this->getMockBuilder(SessionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->once())
            ->method("get")
            ->will($this->returnValue($currency));

        $manager = new CurrencyManager($session);

        $manager->set($currencyItem);

        $this->assertEquals($currency, $manager->get());
    }
}