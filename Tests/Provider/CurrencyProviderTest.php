<?php

namespace eezeecommerce\CurrencyBundle\Tests\Provider;

use eezeecommerce\CurrencyBundle\Core\CurrencyManager;
use eezeecommerce\CurrencyBundle\Provider\CurrencyProvider;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use eezeecommerce\CurrencyBundle\Entity\Currency;
use eezeecommerce\SettingsBundle\Entity\Settings;
use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;

class CurrencyProviderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var EventDispatcherInterface
     */
    private $event;

    /**
     * @var SettingsProvider
     */
    private $settings;

    /**
     * @var CurrencyManager
     */
    private $manager;

    public function setUp()
    {
        $this->event = $this->getMockBuilder(EventDispatcherInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->settings = $this->getMockBuilder(SettingsProvider::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->manager = $this->getMockBuilder(CurrencyManager::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testCurrencyProviderRequiresThreeArgs()
    {
        $provider = new CurrencyProvider($this->event, $this->settings, $this->manager);

        $this->assertNotFalse($provider);
    }

    public function testCurrencyReturnsValidCurrencyItem()
    {
        $currency = $this->getMockBuilder(Currency::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settings = $this->getMockBuilder(Settings::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settings->expects($this->once())
            ->method("getDefaultCurrency")
            ->will($this->returnValue($currency));

        $this->settings->expects($this->any())
            ->method("loadSettingsBySite")
            ->will($this->returnValue($settings));

        $this->manager->expects($this->any())
            ->method("set")
            ->will($this->returnValue(null));

        $currencyItem = $this->getMockBuilder(CurrencyItem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $currencyItem->expects($this->any())
            ->method("getEntity")
            ->will($this->returnValue($currency));

        $this->manager->expects($this->any())
            ->method("get")
            ->will($this->returnValue($currencyItem));

        $provider = new CurrencyProvider($this->event, $this->settings, $this->manager);

        $result = $provider->loadCurrency();

        $this->assertEquals($currencyItem, $result);

        $this->assertEquals($currency, $result->getEntity());
    }
}