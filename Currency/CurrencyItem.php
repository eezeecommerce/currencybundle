<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/05/16
 * Time: 14:53
 */

namespace eezeecommerce\CurrencyBundle\Currency;


use eezeecommerce\CurrencyBundle\Entity\Currency;

class CurrencyItem
{
    /**
     * @var Currency
     */
    private $currency;

    public function __construct(Currency $currency)
    {
        $this->currency = $currency;
    }

    /**
     * Return Currency Entity
     *
     * @return Currency
     */
    public function getEntity()
    {
        return $this->currency;
    }
}