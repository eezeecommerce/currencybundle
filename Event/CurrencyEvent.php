<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/05/16
 * Time: 14:07
 */

namespace eezeecommerce\CurrencyBundle\Event;


use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use Symfony\Component\EventDispatcher\Event;

class CurrencyEvent extends Event
{
    /**
     * @var CurrencyItem
     */
    protected $currency;

    public function __construct(CurrencyItem $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return CurrencyItem
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}