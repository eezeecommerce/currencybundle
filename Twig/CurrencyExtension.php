<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15/04/16
 * Time: 11:31
 */

namespace eezeecommerce\CurrencyBundle\Twig;


use NumberFormatter;
use Symfony\Component\Intl\Intl;

class CurrencyExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            "currencySymbol" => new \Twig_Function_Method($this, "currencySymbolFunction"),
        );
    }

    public function currencySymbolFunction($currency)
    {
        $locale = \Locale::getDefault();
        $symbol = Intl::getCurrencyBundle()->getCurrencySymbol($currency, $locale);
        return $symbol;
    }

    public function getName()
    {
        return "currency_extension";
    }
}