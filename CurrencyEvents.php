<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/05/16
 * Time: 14:58
 */

namespace eezeecommerce\CurrencyBundle;


class CurrencyEvents
{
    const CURRENCY_SAVE_INITIALISE = "eezeecommerce.currency.save.initialise";

    const CURRENCY_SAVE_COMPLETED = "eezeecommerce.currency.save.completed";
}